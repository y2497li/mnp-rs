/*
 * A Multi-Way Number Partitioning library
 * Copyright 2019 Yunxiang Li
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#![feature(test)]

extern crate test;

pub trait Cost {
    fn cost(&self) -> usize;
}

impl Cost for usize {
    fn cost(&self) -> usize {
        *self
    }
}

impl<T: Cost> Cost for &T {
    fn cost(&self) -> usize {
        (**self).cost()
    }
}

pub mod cga;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cga() {
        assert_eq!(cga::solve(1, vec![1, 3, 2]), vec![vec![1, 3, 2]]);
        assert_eq!(cga::solve(2, vec![1, 3, 2]), vec![vec![3], vec![2, 1]]);
        assert_eq!(
            cga::solve(3, vec![1, 3, 2]),
            vec![vec![1], vec![3], vec![2]]
        );
        assert_eq!(
            cga::solve(4, vec![1, 3, 2]),
            vec![vec![1], vec![3], vec![2]]
        );
        assert_eq!(
            cga::solve(3, vec![8, 4, 7, 5, 6]),
            vec![vec![8], vec![7, 4], vec![6, 5]]
        );
        assert_eq!(
            cga::solve(4, vec![8, 4, 7, 5, 6]),
            vec![vec![6], vec![7], vec![8], vec![5, 4]]
        );
    }

    use test::Bencher;
    #[bench]
    fn test_cga_big(b: &mut Bencher) {
        // random big numbers, to avoid perfect partition
        b.iter(|| {
            assert_eq!(
                cga::solve(
                    8,
                    vec![
                        32525, 17303, 7084, 24185, 2233, 23788, 20717, 25841, 14545, 14807, 30030,
                        23001, 310, 10096, 27283, 8125, 28625, 21273, 19109, 25831, 3628, 25627,
                        15169, 26692,
                    ]
                ),
                vec![
                    vec![25841, 15169, 14807],
                    vec![26692, 19109, 10096],
                    vec![25831, 23001, 7084],
                    vec![30030, 25627, 310],
                    vec![32525, 21273, 2233],
                    vec![24185, 17303, 14545],
                    vec![28625, 23788, 3628],
                    vec![27283, 20717, 8125]
                ]
            )
        });
    }

    #[bench]
    fn test_cga_many(b: &mut Bencher) {
        // random small numbers, realistic in some applications
        b.iter(|| {
            assert_eq!(
                cga::solve(
                    8,
                    vec![
                        13, 151, 172, 121, 185, 236, 237, 241, 209, 215, 78, 217, 54, 112, 147,
                        189, 209, 25, 165, 231, 44, 27, 65, 68, 150, 127, 241, 129, 235, 85, 49,
                        248, 236, 220, 112, 164, 199, 92, 148, 152, 50, 225, 112, 103, 80, 2, 36,
                        32, 26, 200, 6, 70, 227, 71, 137, 120, 197, 122, 248, 175, 206, 40, 166,
                        185, 3, 22, 92, 201, 113, 239, 96, 163, 208, 207, 9, 31, 208, 44, 63, 234,
                        244, 68, 47, 214, 138, 183, 77, 78, 48, 68, 253, 254, 107, 162, 182, 109,
                        183, 18, 53, 40,
                    ]
                ),
                vec![
                    vec![248, 231, 209, 206, 166, 163, 121, 109, 78, 54, 49, 27],
                    vec![241, 236, 217, 189, 183, 151, 129, 103, 85, 53, 48, 26],
                    vec![241, 236, 215, 199, 172, 162, 113, 112, 78, 68, 40, 25],
                    vec![254, 225, 214, 197, 182, 148, 122, 112, 71, 68, 47, 13, 9],
                    vec![253, 227, 208, 207, 165, 164, 120, 112, 70, 68, 44, 18, 6],
                    vec![248, 234, 208, 200, 175, 152, 137, 96, 80, 65, 36, 31],
                    vec![244, 235, 209, 201, 183, 147, 127, 107, 77, 63, 44, 22, 3],
                    vec![239, 237, 220, 185, 185, 150, 138, 92, 92, 50, 40, 32, 2]
                ]
            )
        });
    }
}
